// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('printingApp', ['ionic', 'ionic-material', 'ngCordova', 'PubSub', 'pascalprecht.translate'])

.run(['$rootScope', '$ionicPlatform', function($rootScope, $ionicPlatform) {
    $rootScope.$on('$translateChangeSuccess', function() {
        console.log('Selected language applied');
    });

    $rootScope.$on('$translateChangeError', function() {
        alert('Error, selected language was not applied');
    });


    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
    $rootScope._ = window._;
}])

.config(['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider', '$translateProvider', function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $translateProvider) {
    $ionicConfigProvider.backButton.previousTitleText(false);
    $ionicConfigProvider.backButton.icon('ion-chevron-left');
    $ionicConfigProvider.backButton.text('');

    if (localStorage.lang) {
        $translateProvider.preferredLanguage(localStorage.lang);
    } else {
        $translateProvider.preferredLanguage('en');
    }

    $translateProvider.registerAvailableLanguageKeys(['en', 'fr'], {
        'en-*': 'en',
        'fr-*': 'fr'
    });

    $translateProvider.useStaticFilesLoader({
        prefix: 'js/data/locale-',
        suffix: '.json'
    });

    $translateProvider.useSanitizeValueStrategy(null);

    $stateProvider
        .state('login', {
            url: '/login',
            cache: false,
            templateUrl: 'templates/views/login.html',
            controller: 'LoginController as vm',

        })

    .state('app', {
        url: '/app',
        abstract: true,
        controller: 'MainController as vm',
        templateUrl: 'templates/main.html'
    })

    .state('app.sharing', {
        url: '/sharing',
        cache: false,
        views: {
            'viewContent': {
                templateUrl: 'templates/views/shareFile.html',
                controller: 'sharingController as vm',
            }
        }
    })

    .state('app.myfiles', {
        url: '/myfiles',
        cache: false,
        views: {
            'viewContent': {
                templateUrl: 'templates/views/myFiles.html',
                controller: 'myFilesController as vm'
            }
        }
    })


    .state('app.credit', {
        url: '/credit',
        cache: true,
        views: {
            'viewContent': {
                templateUrl: 'templates/views/credit.html',
                controller: 'CreditController as vm'
            }
        }
    })

    .state('app.settings', {
        url: '/settings',
        cache: true,
        views: {
            'viewContent': {
                templateUrl: 'templates/views/settings.html',
                controller: 'SettingsController as vm'
            }
        }
    })

    .state('app.home', {
        url: '/home',
        cache: true,
        views: {
            'viewContent': {
                templateUrl: 'templates/views/home.html',
                controller: 'homeController as vm'
            }
        }
    });

    // redirects to default route for undefined routes
    $urlRouterProvider.otherwise('/login');
}]);