(function() {
    'use strict';
    angular.module('printingApp')

    .factory('facebookAuthService', facebookAuthService);
    facebookAuthService.$inject = ['$q', '$ionicLoading'];

    function facebookAuthService($q, $ionicLoading) {
        var service = {
            getLoginStatus: getLoginStatus,
            loginFacebook: loginFacebook,
            logoutFacebook: logoutFacebook,
            getFacebookApi: getFacebookApi
        }

        return service;

        /////

        function getLoginStatus() {
            var defer = $q.defer();

            FB.getLoginStatus(function(response) {

                if (response.status === "connected") {
                    defer.resolve(response);
                } else {
                    console.log("Not logged in");
                }
            });

            return defer.promise;
        }


        function loginFacebook() {
            var defer = $q.defer();

            FB.login(function(response) {

                if (response.status === "connected") {
                    defer.resolve(response);
                } else {
                    console.log("Not logged in!");
                }
            });

            return defer.promise;
        }

        function logoutFacebook() {
            var defer = $q.defer();

            FB.logout(function(response) {
                defer.resolve(response);
            });

            return defer.promise;
        }

        function getFacebookApi() {
            var defer = $q.defer();

            FB.api("/me?fields=name,email", [], function(response) {

                if (response.error) {
                    defer.reject(response);
                } else {
                    defer.resolve(response);
                }
            });

            return defer.promise;
        }

    }
})();